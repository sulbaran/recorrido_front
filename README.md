# Recorrido
## Desafío Técnico

## Instalación

Require [Vue Cli] [https://cli.vuejs.org/guide/installation.html]

Instalar dependencias

```sh
cd recorrido_front
npm install
```

Para ejecutar el servidor

```sh
npm run serve
```

## Plugins

Se utilizaron los siguientes Plugins

| Plugin |
| ------ |
| Bootstrap |
| Vue Route |
| Swalert2 |