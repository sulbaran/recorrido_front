import { createRouter, createWebHistory } from 'vue-router'
import LoginView from '../views/LoginView.vue'
import HomeView from '../views/HomeView.vue'
import HoursAvailable from '../views/HoursAvailable.vue'
import HoursConfirmed from '../views/HoursConfirmed.vue'

const routes = [
  {
    path: '/',
    name: 'login',
    component: LoginView
  },
  {
    path: '/home',
    name: 'home',
    component: HomeView,
    props: route => ({ token: route.query.token })
  },
  {
    path: '/hours_available',
    name: 'hours_available',
    component: HoursAvailable
  },
  {
    path: '/hours_confirmed',
    name: 'hours_confirmes',
    component: HoursConfirmed
  }
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router
